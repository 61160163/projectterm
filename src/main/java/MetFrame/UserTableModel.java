/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MetFrame;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.User;

/**
 *
 * @author Admin
 */
public class UserTableModel extends AbstractTableModel {

    private ArrayList<User> user;
    private String[] col = {"ID", "Name", "LastName", "Status"};

    public UserTableModel(ArrayList<User> user) {
        this.user = user;
    }

    public int getRowCount() {
        return this.user.size();
    }

    @Override

    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User user = this.user.get(rowIndex);
        if (columnIndex == 0) {
            return user.getId();
        }
        if (columnIndex == 1) {
            return user.getName();
        }
        if (columnIndex == 2) {
            return user.getLname();
        }
        if (columnIndex == 3) {
            return user.getStatus();
        }
       
        return "";
    }

    @Override
    public String getColumnName(int column) {
        return col[column];
    }

}
