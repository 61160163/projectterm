package MetFrame;


import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.Customer;
import model.Employee;
import model.Order;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Admin
 */
class OrderTableModel extends AbstractTableModel{

    private ArrayList<Order> order;
    
    private String[] col = {"Order ID", "Customer Name", "Employee Name","Total"};

    public OrderTableModel(ArrayList<Order> order) {
        this.order = order;
        
    }

    public int getRowCount() {
        return this.order.size();
    }

    @Override

    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Order order = this.order.get(rowIndex);
        if (columnIndex == 0) {
            return order.getId();
        }
        
         if (columnIndex == 1) {
            return order.getCusName();
        }
       
        if (columnIndex == 2) {
            return order.getEmpName();
        }
        if (columnIndex == 3) {
            return order.getTotal();
        }
        
        return "";
    }

    @Override
    public String getColumnName(int column) {
        return col[column];
    }
    
}
