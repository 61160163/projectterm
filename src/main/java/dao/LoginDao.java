/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Login;

/**
 *
 * @author user
 */
public class LoginDao implements DaoInterface<Login> {

    @Override
    public int add(Login object) {
        return 0;
    }

    @Override
    public ArrayList<Login> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT login_username,login_password,user_id FROM login";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                String userName = result.getString("login_username");
                String password= result.getString("login_password");
                int user_id=result.getInt("user_id");
                Login login = new Login(userName,password,user_id);
                list.add(login);
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by this :"+ex);
        }
        db.close();
        return list;
    }

    @Override
    public Login get(int id) {
        return null;
    }

    @Override
    public int delete(int id) {
        return 0;
    }

    @Override
    public int update(Login object) {
        return 0;
    }
    
}
