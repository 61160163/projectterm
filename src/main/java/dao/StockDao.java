/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Stock;

/**
 *
 * @author User
 */
public class StockDao implements DaoInterface<Stock> {
    
    //private static Stock stock;
    
    public static void main(String[] args) {

    }

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO stock (stock_name, stock_amount)VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getAmount());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: " + ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList stocklist = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT `stock_id` , `stock_name` , `stock_amount` FROM Stock";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int stock_id = result.getInt("stock_id");
                String stock_name = result.getString("stock_name");
                int stock_amount = result.getInt("stock_amount");
                Stock stock = new Stock(stock_id , stock_name , stock_amount);
                stocklist.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println("Error: :" + ex);
        }
        db.close();
        return stocklist;
    }
    

    @Override
    public Stock get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT * FROM Stock WHERE stock_id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int stock_id = result.getInt("stock_id");
                String stock_name = result.getString("stock_name");
                int stock_amount = result.getInt("stock_amount");
                Stock stock = new Stock(stock_id , stock_name , stock_amount);
                return stock;
            }
        } catch (SQLException ex) {
            System.out.println("Error: : " + ex.getMessage());
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int row = 0;
        try {
            String sql = "DELETE FROM Stock WHERE stock_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : " + ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int row = 0;
        try {
            String sql = "UPDATE stock SET stock_name = ? , stock_amount = ? WHERE stock_id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getAmount());
            stmt.setInt(3, object.getStockid());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : " + ex.getMessage());
        }
        db.close();
        return row;
    }

    public Stock search(String name) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT * FROM Stock WHERE stock_name LIKE '%" + name + "%'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int stock_id = result.getInt("stock_id");
                String stock_name = result.getString("stock_name");
                int stock_amount = result.getInt("stock_amount");
                Stock stock = new Stock(stock_id , stock_name , stock_amount);
                return stock;
            }
        } catch (SQLException ex) {
            System.out.println("Error: : " + ex.getMessage());
        }
        db.close();
        return null;
    }
    
}
