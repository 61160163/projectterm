/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Customer;

/**
 *
 * @author LENOVO
 */
public class CustomerDao implements DaoInterface<Customer> {

    private static Customer Customer;

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Customer (cus_name, cus_lname, cus_tel)VALUES (?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getCusName());
            stmt.setString(2, object.getCusLastname());
            stmt.setString(3, object.getCusTel());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: fail to add");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT cus_id,cus_name,cus_lname,cus_tel FROM Customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int cus_id = result.getInt("cus_id");
                String cus_name = result.getString("cus_name");
                String cus_lname = result.getString("cus_lname");
                String cus_tel = result.getString("cus_tel");
                Customer customer = new Customer(cus_id, cus_name, cus_lname, cus_tel);
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by this :" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT cus_id,cus_name,cus_lname,cus_tel FROM Customer WHERE cus_id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int cus_id = result.getInt("cus_id");
                String cus_name = result.getString("cus_name");
                String cus_lname = result.getString("cus_lname");
                String cus_tel = result.getString("cus_tel");
                Customer customer = new Customer(cus_id, cus_name, cus_lname, cus_tel);
                return customer;
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by this :" + ex.getMessage());
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int row = 0;
        try {
            String sql = "DELETE FROM Customer WHERE cus_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :" + ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        int row = 0;
        try {
            String sql = "UPDATE Customer SET cus_name = ?,cus_lname = ?,cus_tel = ? WHERE cus_id = ?";

            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getCusName());
            stmt.setString(2, object.getCusLastname());
            stmt.setString(3, object.getCusTel());
            stmt.setInt(4, object.getCusId());
            
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :" + ex.getMessage());
        }
        db.close();
        return row;
    }

    public Customer getByPhone(String phoneNum) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT cus_id ,cus_name ,cus_lname ,cus_tel FROM Customer WHERE cus_tel LIKE" + "'" + phoneNum + "%" + "'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int cus_id = result.getInt("cus_id");
                String cus_name = result.getString("cus_name");
                String cus_lname = result.getString("cus_lname");
                String cus_tel = result.getString("cus_tel");
                Customer customer = new Customer(cus_id, cus_name, cus_lname, cus_tel);
                return customer;
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by this :" + ex.getMessage());
        }
        db.close();
        return null;
    }

    public static void main(String[] args) {
        Customer c = new Customer(2, "Somchay", "Jaikara", "0999999998");
        CustomerDao dao = new CustomerDao();
        //dao.add(c);
        dao.update(c);
        System.out.println(dao.get(2));
    }
}
