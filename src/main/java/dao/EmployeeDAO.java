/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;
import model.User;

/**
 *
 * @author ASUS
 */
public class EmployeeDAO implements DaoInterface<Employee> {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Employee (emp_name,emp_lname,emp_tel,emp_salary) VALUES (?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getLname());
            stmt.setString(3, object.getTel());
            stmt.setDouble(4, object.getSalary());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: fail to add");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT emp_id,emp_name,emp_lname,emp_tel,emp_salary FROM Employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int emp_id = result.getInt("emp_id");
                String emp_name = result.getString("emp_name");
                String emp_lname = result.getString("emp_lname");
                String emp_tel = result.getString("emp_tel");
                double emp_salary = result.getDouble("emp_salary");
                Employee employee = new Employee(emp_id, emp_name, emp_lname, emp_tel, emp_salary);
                list.add(employee);
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by this :" + ex);
        }
        db.close();
        return list;
    }
    public ArrayList<Employee> getSearch(String object) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT emp_id,emp_name,emp_lname,emp_tel,emp_salary FROM Employee WHERE emp_id LIKE"+"'"+object+"%"+"'"+"OR emp_name Like"+"'"+object+"%"+"'";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int emp_id = result.getInt("emp_id");
                String emp_name = result.getString("emp_name");
                String emp_lname = result.getString("emp_lname");
                String emp_tel = result.getString("emp_tel");
                double emp_salary= result.getDouble("emp_salary");
                Employee emp = new Employee(emp_id, emp_name, emp_lname, emp_tel,emp_salary);
                list.add(emp);
            }
        } catch (SQLException ex) {
            System.out.println("Error: getName" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT emp_id,emp_name,emp_lname,emp_tel,emp_salary FROM Employee WHERE emp_id = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int emp_id = result.getInt("emp_id");
                String emp_name = result.getString("emp_name");
                String emp_lname = result.getString("emp_lname");
                String emp_tel = result.getString("emp_tel");
                double emp_salary = result.getDouble("emp_salary");
                Employee employee = new Employee(emp_id, emp_name, emp_lname, emp_tel, emp_salary);
                return employee;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR: fail to get data");
        }
        return null;
    }

    @Override
    public int delete(int id) {
         Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM employee WHERE emp_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();           
        } catch (SQLException ex) {
            System.out.println("Error: delete "+ex.getMessage());
        }
        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Employee SET emp_name=?, emp_lname=?, emp_tel=?, emp_salary=?  WHERE emp_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getLname());
            stmt.setString(3, object.getTel());
            stmt.setDouble(4, object.getSalary());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR: fail to update");
            System.out.println(object.toString());
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        
    }
}
