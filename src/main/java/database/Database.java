/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author user
 */
public class Database {
    
    private static Database instance = new Database();
    private Connection conn;
    
    private Database() {
    }

    public static Database getInstance() {

        String dbPath = "./db/coffee.db";
        try {
            
            if (instance.conn == null || instance.conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Databest Connection");
            }
        } catch (ClassNotFoundException xe) {
            System.out.println("Eror: JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Eror: Database cannot connection");
        }
        return instance;
    }
    
    public static void close() {
        try {
            if (instance.conn != null && !instance.conn.isClosed()) {
                instance.conn.close();
                
            }
        } catch (SQLException ex) {
            System.out.println("Error: yes is error.");
        }
        instance.conn = null;
    }
    
    public Connection getConnection(){
        return instance.conn;
    }
    
}
