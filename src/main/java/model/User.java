/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author user
 */
public class User {
    private int id;
    private String name;
    private String lname;
    private String status;
    private int emp_id;

    public User(int id, String name, String lname,String status) {
        this.id = id;
        this.name = name;
        this.lname = lname;
        this.status = status;

    }
        public User(int id, String name, String lname,String status,int emp_id) {
        this.id = id;
        this.name = name;
        this.lname = lname;
        this.status = status;
        this.emp_id=emp_id;
    }
    public User(int userId, String userName, String userTel) {
        this(userId, userName, userTel, "");
    }
    
    public User(String name, String tel, String password) {
        this(-1, name, tel, password);
    }
    
     public User(String name, String tel) {
        this(-1, name, tel,"");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public int getEmpId(){
        return emp_id;
    }

    @Override
    public String toString() {
        return "User" + "id: " + id + ", name: " + name + ", lname:" + lname + ", type: " + ", status: " + status + " " ;
    }
    
    
}
