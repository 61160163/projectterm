/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author user
 */
public class Login {
    private int loginID;
    private String loginUsername;
    private String loginPassword;
    private int userID;

    public Login(int loginID, String loginUsername, String loginPassword) {
        this.loginID = loginID;
        this.loginUsername = loginUsername;
        this.loginPassword = loginPassword;
    }

    public Login(String loginUsername, String loginPassword,int user_id) {
        this.loginUsername = loginUsername;
        this.loginPassword = loginPassword;
        this.userID=user_id;
    }

    public int getLoginID() {
        return loginID;
    }

    public void setLoginID(int loginID) {
        this.loginID = loginID;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginName) {
        this.loginUsername = loginName;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return "Login{" + "loginID=" + loginID + ", loginUsername=" + loginUsername + ", loginPassword=" + loginPassword + ", userID=" + userID + '}';
    }
    
}
